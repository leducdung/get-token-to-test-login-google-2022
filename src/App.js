import React, { useEffect, useState } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { gapi } from 'gapi-script';

function AuthPage() {
  const [data, setData] = useState({});

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: '1015624852230-eq285hov8k6fk9rni4qac1qqj61c695b.apps.googleusercontent.com',
        scope: 'email',
      });
    }

    gapi.load('client:auth2', start);
  }, []);


  // **you can access the token like this**
  // const accessToken = gapi.auth.getToken().access_token;
  // console.log(accessToken);

  const onSuccess = response => {
    console.log('SUCCESS', response);
    setData(response)
  };
  const onFailure = response => {
    console.log('FAILED', response);
  };
  const onLogoutSuccess = () => {
    console.log('SUCESS LOG OUT');
  };


  return (
    <div>
      <GoogleLogin
        clientId='1015624852230-eq285hov8k6fk9rni4qac1qqj61c695b.apps.googleusercontent.com'
        onSuccess={onSuccess}
        onFailure={onFailure}
      />
      <GoogleLogout
        clientId='1015624852230-eq285hov8k6fk9rni4qac1qqj61c695b.apps.googleusercontent.com'
        onLogoutSuccess={onLogoutSuccess}
      />
      {
        data?.tokenId
      }
 
    </div>
  );
}

export default AuthPage;